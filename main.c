/**
  * Program Ukolovnicek
  *
  * Popis:
  * Zaznamenava do dynamickeho pole struktur jednotlive udalosti s datem,hodinou, prioritou a nazvem,
  * umi je vypisovat.
  *
  * Autor: Stanislav Marek
  * Datum: 2.2.2014
  *
  */
//#include "stdafx.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h> //kvuli konstante USHRT_MAX pri nacitani data


enum {
    NE = 0,
    ANO = 1
};


typedef struct { // typ struktura t_datum
    unsigned short int den;
    unsigned short int mesic;
    unsigned short int rok;
    unsigned short int hodina;
} t_datum;


typedef struct { // typ struktura
    t_datum datum;
    char* nazev;
    unsigned short int dulezitost;
} t_udalost;


/* Funkce pridej_udalost - prida udalost do struktury t_udalost
 *
 * @param unsigned short den - den
 * @param unsigned short mesic - mesic
 * @param unsigned short rok - rok
 * @param unsigned short hodina - hodina
 * @param unsigned short dulezitost - dulezitost (priorita)
 * @param char* nazev - nazev udalosti
 *
 * @return t_udalost udalost - vraci ukazatel na strukturu obsahujici udalost
 */
t_udalost pridej_udalost(unsigned short den,
                         unsigned short mesic,
                         unsigned short rok,
                         unsigned short hodina,
                         unsigned short dulezitost,
                         char* nazev){

    t_udalost udalost; //vytvorime si udalost

    int delka_nazvu = strlen(nazev); //ulozime delku nazvu do promenne (omezime pocet volani strlen dale ve funkci)

    if((udalost.nazev = (char*)malloc(delka_nazvu+1)) == NULL){ //alokujeme misto na nazev
        fprintf(stderr, "Nepovedlo se alokovat misto pro nazev udalosti, koncim.\n");
        exit(EXIT_FAILURE);
    }

    //zkopirujeme data do struktury
    udalost.datum.den = den;
    udalost.datum.mesic = mesic;
    udalost.datum.rok = rok;
    udalost.datum.hodina = hodina;
    udalost.dulezitost = dulezitost;
    strncpy(udalost.nazev, nazev, delka_nazvu);
    udalost.nazev[delka_nazvu] = '\0'; //pridame ukoncovaci nulu

    return udalost;
}


/* Funkce uvolni_nazvy - projde pole a uvolni nazvy jednotlivych polozek
 *
 * @param t_udalost *pole - pole prvku k uvolneni
 * @param unsigned int pocet - pocet prvku k uvolneni
 */
void uvolni_nazvy(t_udalost *pole, unsigned int pocet){

    for(unsigned int i = 0; i < pocet; ++i){
        free((void*)pole[i].nazev);
    }
}


/* Funkce uvolni - projde pole a zavola funkci, ktera uvolni nazvy, a nakonec uvolni i cele pole
 *
 * @param t_udalost *pole - pole prvku k uvolneni
 * @param unsigned int pocet - pocet prvku k uvolneni
 */
void uvolni(t_udalost *pole, unsigned int pocet){

    uvolni_nazvy(pole, pocet);
    free(pole);
}


/* Funkce prohod - prohazuje 2 pole struktur t_udalost
 *
 * @param t_udalost *cil - pole struktur do ktereho ukladame
 * @param t_udalost *zdroj - pole struktur ze ktereho nacitame
 * @param unsigned int pocet - pocet prvku k prohozeni
 */
void prohod (t_udalost *cil, t_udalost *zdroj, unsigned int pocet){

    for(unsigned int i = 0; i < pocet; ++i){

        int delka_nazvu = strlen(zdroj[i].nazev);

        if((cil[i].nazev = malloc(delka_nazvu+1)) == NULL){
            fprintf(stderr, "Nepovedlo se alokovat nazev pro tmp pri prehazovani, koncim->\n");
            exit(EXIT_FAILURE);
        }

        cil[i].dulezitost = zdroj[i].dulezitost;
        strncpy(cil[i].nazev, zdroj[i].nazev, delka_nazvu);
        cil[i].nazev[delka_nazvu] = '\0';
        cil[i].datum = zdroj[i].datum;
    }
}


/* Funkce realokuj - prida do daneho pole o prvek navic
 *
 * @param t_udalost **pole - pole struktur
 * @param unsigned int pocet prvku v poli
 */
void realokuj(t_udalost **pole, unsigned int pocet){

    t_udalost *tmp; //ukazatel na docasne pole do ktereho se zkopiruje puvodni pole


    if((tmp = (t_udalost*)malloc(pocet * sizeof(t_udalost))) == NULL){
        fprintf(stderr, "Nepovedlo se alokovat pole tmp pro realokaci, koncim.\n");
        exit(EXIT_FAILURE);
    }

    prohod(tmp, *pole, pocet); //zkopiruje puvodni pole do docasneho
    uvolni_nazvy(*pole, pocet); //uvolni nazvy v puvodnim poli

    if((*pole = (t_udalost*)realloc(*pole, (pocet+1) * sizeof(t_udalost))) == NULL){ //zvetsi puvodni pole o dalsi prvek
        fprintf(stderr, "Nepovedlo se realokovat pole s udalostmi, koncim.\n");
        exit(EXIT_FAILURE);
    }

    prohod(*pole, tmp, pocet); //zkopiruje docasne pole zpet do puvodniho
    uvolni(tmp, pocet); //uvolni docasne pole
}


/* Funkce vypis - vypisuje obsah pole struktur
 *
 * @param t_udalost *pole - pole prvku k vypsani
 * @param unsigned int pocet - pocet prvku k vypsani
 * @param int vypis_cisla - urcuje jestli maji byt vypsana poradova cisla v poli ci nikoliv
 */
void vypis(t_udalost *pole, unsigned int pocet, int vypis_cisla){

    if(vypis_cisla){ //pokud ma vypsat i poradova cisla v poli, vypise prvky i s nimi

        printf("poradi\tdatum\t\tcas\tdulezitost\tnazev\n");

        for(unsigned int i = 0; i < pocet; ++i){
            printf("[%d]\t%hu.%hu.%hu\t%hu\t%hu\t\t%s\n", i,
                                                    pole[i].datum.den,
                                                    pole[i].datum.mesic,
                                                    pole[i].datum.rok,
                                                    pole[i].datum.hodina,
                                                    pole[i].dulezitost,
                                                    pole[i].nazev);
        }
    }
    else { //jinak vypise prvky bez poradovych cisel

        printf("datum\t\tcas\tdulezitost\tnazev\n");

        for(unsigned int i = 0; i < pocet; ++i){
            printf("%hu.%hu.%hu\t%hu\t%hu\t\t%s\n", pole[i].datum.den,
                                                    pole[i].datum.mesic,
                                                    pole[i].datum.rok,
                                                    pole[i].datum.hodina,
                                                    pole[i].dulezitost,
                                                    pole[i].nazev);
        }
    }
}


/* Funkce smaz - smaze udalost z pole
 *
 * @param t_udalost **pole - ukazatel na pole ze ktereho mazeme
 * @param unsigned int id - poradove cislo prvku, ktery chceme smazat
 * @param unsigned int *pocet - ukazatel na pocet prvku v poli
 */
void smaz(t_udalost **pole, unsigned int id, unsigned int *pocet){

    *pocet-=1;

    for(unsigned int i = id; i < *pocet; ++i){
        pole[i] = pole[i+1];
    }
    realokuj(pole, *pocet);
}


/* Funkce nacti_hodnotu - nacita hodnotu dokud neni splnena dana podminka
 *
 * @param unsigned short int *cil - ukazatel na promennou, do ktere nacitame
 * @param unsigned short int podminka - podminka ktera musi byt splnena
 * @param char *nazev - nazev veliciny, kterou ukladame do promenne
 */
void nacti_hodnotu(unsigned short int *cil, unsigned short int podminka, char *nazev){

    int neplatne = 0; //pomocna promenna na urceni platnosti zadane hodnoty

    do {
        if(neplatne) //pokud byla minula hodnota neplatna, vypiseme hlasku a nacitame znovu
            printf("\nNeplatny %s, zkuste to znovu.\n\n", nazev);

        printf("Zadejte %s: ", nazev);
        fflush(stdin);
        scanf("%hu", cil);
//        scanf_s("%hu", cil, sizeof(cil));

        neplatne++; //nastavime hodnotu jako neplatnou, pokud je splnena podminka, nic se nestane, pokud neni, vypise se chybova hlaska a nacitame znovu
    }
    while(*cil > podminka);
}


/* Funkce nacti_hodnotu - nacita hodnotu dokud neni splnena dana podminka
 *
 * @param unsigned short int *den - ukazatel na promennou, do ktere nacitame den
 * @param unsigned short int *mesic - ukazatel na promennou, do ktere nacitame mesic
 * @param unsigned short int *rok - ukazatel na promennou, do ktere nacitame rok
 * @param unsigned short int *hodina - ukazatel na promennou, do ktere nacitame hodinu
 */
void nacti_datum(unsigned short int *den, unsigned short int *mesic, unsigned short int *rok, unsigned short int *hodina){

    int neplatne = 0; //pomocna promenna na urceni platnosti data

    while(1){
        if(neplatne) //pokud bylo minule datum neplatne, vypiseme hlasku a zkusime nacist znovu
            printf("\nNeplatne datum, zkuste to znovu.\n\n");

        nacti_hodnotu(den, 31, "den"); //nejvyssi pocet dni v mesici muze byt 31
        nacti_hodnotu(mesic, 12, "mesic"); //nejvyssi pocet mesicu v roce muze byt 12
        nacti_hodnotu(rok, USHRT_MAX, "rok"); //nejvyssi pocet roku nemame, jako limit tedy zvolime limit datoveho typu
        nacti_hodnotu(hodina, 24, "hodinu"); //den ma nejvyse 24 hodin

        if(*mesic == 2){ //pokud jsme nacetli unor
            /* A pokud jsme nacetli 29. unor, overime, zda je prestupny rok (prestupny je kazdy 4 rok,
             * ale kazdy 100. rok prestupny neni. Kazdy 400. rok ale prestupny je.*/
            if(*den == 29 && ((*rok % 4 != 0 || *rok % 100 == 0) && *rok % 400 != 0))
                neplatne++; //pokud neni prestupny rok (podminka je splnena), nastavime datum jako neplatne
            else break; //jinak je prestupny a mame platne datum, muzeme tedy cyklus zadavani platneho data ukoncit
        }
        // Pokud mame nektery z mesicu trvajicich 30 dni, ale nacetli jsme 31. den, nastavime datum jako neplatne
        else if((*mesic == 4 || *mesic == 6 || *mesic == 9 || *mesic == 11) && *den > 30){
            neplatne++;
        }
        else break; //jinak mame platne datum, muzeme tedy cyklus zadavani platneho data ukoncit
    }
}


/* Funkce vycisti_obrazovku - vycisti obrazovku terminalu
 */
void vycisti_obrazovku(void){

    if (system("CLS")) system("clear");
    fflush(stdin);
}



int main(void){

    unsigned int nactenych = 0; //pocet nactenych prvku v poli
    t_udalost *udalosti; //pole udalosti
    udalosti = NULL;

    while(1){
        printf("\nVyber akci:\n\tPridej udalost\t[p]\n\tVypis udalosti\t[v]\n\tSmazat udalost\t[s]\n\tVymaz obrazovku\t[c]\n\tUkonci program\t[k]\n");
        fflush(stdin);
        char c = getchar(); //zjistime, co po programu chceme

        unsigned short int den = 0,
                           mesic = 0,
                           rok = 0,
                           hodina = 0,
                           dulezitost = 0,
                           ukoncit = 0;
        char nazev[101];
        unsigned int id_ke_smazani;

        switch (c) {
          case 'p': //pridat udalost
            if(nactenych == 0){ //pokud jsme jeste nic nenacetli, musime vytvorit v pameti misto pro udalost
                if((udalosti = (t_udalost*)malloc(sizeof(t_udalost))) == NULL){
                    fprintf(stderr, "Nepovedlo se alokovat pole udalosti, koncim.\n");
                    exit(EXIT_FAILURE);
                }
            }
            else { //pokud jiz nejaka udalost nactena je, pouze pole rozsirime
                realokuj(&udalosti, nactenych);
            }

            nacti_datum(&den, &mesic, &rok, &hodina);

            printf("Zadejte dulezitost: ");
            fflush(stdin);
            scanf("%hu", &dulezitost);
//            scanf_s("%hu", &dulezitost, sizeof(unsigned short int));

            printf("Zadejte nazev: ");
            fflush(stdin);
            scanf(" %100[^\n]", nazev);
//            scanf_s(" %100[^\n]", nazev, sizeof(nazev) - 1);

            udalosti[nactenych] = pridej_udalost(den, mesic, rok, hodina, dulezitost, nazev);
            vycisti_obrazovku();
            nactenych++;
            break;

          case 'v':
            if(nactenych > 0)
                vypis(udalosti, nactenych, NE);
            else {
                printf("Zatim nebyla pridana zadna udalost.\n");
                getchar();
            }
            break;

          case 's':
            if(nactenych > 0){
                vypis(udalosti, nactenych, ANO);

                printf("Zadejte cislo udalosti, kterou chcete smazat: ");
                fflush(stdin);
                scanf("%u", &id_ke_smazani);

                smaz(&udalosti, id_ke_smazani, &nactenych);
            }
            else {
                printf("Zatim nebyla pridana zadna udalost.\n");
                getchar();
            }
            break;

          case 'c':
            vycisti_obrazovku();
            break;

          case 'k':
            if(nactenych > 0)
                uvolni(udalosti, nactenych);

            ukoncit = 1;
            break;

          default:
            fprintf(stderr, "Neplatna akce\n");
            break;
        }

        if(ukoncit) break;
    }

    fflush(stdout);
    return EXIT_SUCCESS; //konstanta z stdlib, je to takhle hezci
}
